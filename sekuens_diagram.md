```plantuml
@startuml
actor       Dosen                   as Dosen
boundary    Shadow_Bluetape         as Shadow_Bluetape
control     Config_email_dosen      as Config_email_dosen
control     Kolektor_Pengumuman     as Kolektor_Pengumuman
entity      Bot                     as Bot
actor       Mahasiswa               as Mahasiswa


Dosen -> Shadow_Bluetape : Mengirim Pengumuman
activate Shadow_Bluetape

Shadow_Bluetape -> Config_email_dosen : Menerima Email dosen
activate Config_email_dosen

Config_email_dosen -> Shadow_Bluetape : Berhasil Verifikasi
deactivate Config_email_dosen

Shadow_Bluetape -> Kolektor_Pengumuman : Simpan email dalam Kolektor
deactivate Shadow_Bluetape
activate Kolektor_Pengumuman

Kolektor_Pengumuman -> Bot : Mengirim Pengumuman
deactivate Kolektor_Pengumuman
activate Bot

Bot -> Mahasiswa : Mengirim Notifikasi dan Pengumuman
activate Mahasiswa

Mahasiswa --> Bot : Menerima Notifikasi & Pengumuman (status read)
deactivate Mahasiswa
deactivate Bot
@enduml

```
